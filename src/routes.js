import Loading from './assets/vue/pages/loading.vue';
import Start from './assets/vue/pages/start.vue';
import Login from './assets/vue/pages/login.vue';
import Register from './assets/vue/pages/register.vue';
import Home from './assets/vue/pages/home.vue';
import Expense from './assets/vue/pages/expense.vue';
import Income from './assets/vue/pages/income.vue';
import Report from './assets/vue/pages/report.vue';
import SavingTips from './assets/vue/pages/savingTip.vue';
import Setting from './assets/vue/pages/setting.vue';
import PanelLeftPage from './assets/vue/pages/panel-left.vue';

import AddCategory from './assets/vue/pages/addcategory.vue';
import AddBudget from './assets/vue/pages/addbudget.vue';
import AddExpense from './assets/vue/pages/addexpense.vue';
import AddIncome from './assets/vue/pages/addincome.vue';
import EditExpense from './assets/vue/pages/editexpense.vue';
import EditIncome from './assets/vue/pages/editincome.vue';

import AddCategory2 from './assets/vue/pages/addcategory2.vue';
import AddBudget2 from './assets/vue/pages/addbudget2.vue';
import AddExpense2 from './assets/vue/pages/addexpense2.vue';
import AddIncome2 from './assets/vue/pages/addincome2.vue';

import ChangePassword from './assets/vue/pages/changepassword.vue';
import ChangeUsername from './assets/vue/pages/changeusername.vue';

import testFramework from './assets/vue/pages/testFramework.vue';

import AboutPage from './assets/vue/pages/about.vue';

import FormPage from './assets/vue/pages/form.vue';
import DynamicRoutePage from './assets/vue/pages/dynamic-route.vue';

import ColorThemes from './assets/vue/pages/color-themes.vue';
import Chat from './assets/vue/pages/chat.vue';

export default [
  {
    path: '/',
    component: Loading
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/start',
    component: Start
  },
  {
    path: '/register',
    component: Register
  },
  {
    name:'home',
    path: '/home',
    component: Home
  },
  {
    name:'expense',
    path: '/expense',
    component: Expense
  },
  {
    path: '/income',
    component: Income
  },
  {
    path: '/report',
    component: Report
  },
  {
    path: '/savingsTip',
    component: SavingTips
  },
  {
    path: '/setting',
    component: Setting
  },
  {
    path: '/panel-left/',
    component: PanelLeftPage
  },

  {
    path: '/categories/popup',
    popup:{
     component: AddCategory
    }
  },

  {
    path: '/budgets/popup',
    popup:{
     component: AddBudget
    }
  },

  {
    path: '/expenses/popup',
    popup:{
     component: AddExpense
    }
  },

  {
    path: '/incomes/popup',
    popup:{
       component: AddIncome
     }
  },

  {
    path: '/categories/popup2',
    popup:{
     component: AddCategory2
    }
  },

  {
    path: '/budgets/popup2',
    popup:{
     component: AddBudget2
    }
  },

  {
    path: '/expenses/popup2',
    popup:{
     component: AddExpense2
    }
  },

  {
    path: '/incomes/popup2',
    popup:{
       component: AddIncome2
     }
  },

  {
    path: '/editExpense/:expense_id',
    component: EditExpense
  },
  {
    path: '/editIncome/:income_id',
    component: EditIncome
  },
  {
    path: '/changepassword',
    component: ChangePassword
  },
  {
    path: '/changeusername',
    component: ChangeUsername
  },






  {
    path: '/about/',
    component: AboutPage
  },




  {
    path: '/form/',
    component: FormPage
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage
  },
  {
    path: '/color-themes/',
    component: ColorThemes
  },
  {
    path: '/chat/',
    component: Chat
  },
];
